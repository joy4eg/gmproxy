//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"net"
	"time"
)

const dialerGMProxyTimeout = 15 * time.Second

type DialerGMProxy struct {
}

func newDialerGMProxy(s *Server) *DialerGMProxy {
	return new(DialerGMProxy)
}

func (d *DialerGMProxy) Tag() string {
	return "Dialer-GMProxy:"
}

func (d *DialerGMProxy) Connected() bool {
	return true
}

func (d *DialerGMProxy) Close() error {
	return nil
}

func (d *DialerGMProxy) DialTCP(network string, laddr, raddr *net.TCPAddr) (net.Conn, error) {
	log.Debug(d, "connecting to %+v ...", raddr)
	return net.DialTimeout(network, raddr.String(), dialerGMProxyTimeout)
}

func (d *DialerGMProxy) Connect() error {
	return nil
}
