//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"net"
	"time"

	"github.com/hashicorp/golang-lru"
	"github.com/joy4eg/resolver"
)

const resolverCacheSize = 256

type resolverResult struct {
	ip  net.IP
	err error
}

type resolverReq struct {
	addr string
	ch   chan *resolverResult
}

type Resolver struct {
	cache *lru.ARCCache
	conf  *resolver.Config
	req   chan *resolverReq
}

func NewResolver(c *Config) *Resolver {
	conf := resolver.Config{
		Servers:  c.DNS.Servers,
		Timeout:  time.Second * time.Duration(c.DNS.Timeout),
		Attempts: 3,
	}
	// Save config for future usage ...
	r := &Resolver{
		conf: &conf,
	}

	if c.DNS.Cache {
		r.cache, _ = lru.NewARC(resolverCacheSize)
		r.req = make(chan *resolverReq, 32)
		go r.worker()
	}

	// Apply global config to resolver
	resolver.SetConfig(conf)
	return r
}

func (r *Resolver) Tag() string {
	return "Resolver:"
}

func (r *Resolver) cacheRead(addr string) (net.IP, bool) {
	if val, ok := r.cache.Get(addr); ok {
		return val.(net.IP), true
	}
	return nil, false
}

func (r *Resolver) cacheWrite(addr string, ip net.IP) {
	r.cache.Add(addr, ip)
}

func (r *Resolver) worker() {
	type result struct {
		addr string
		ip   net.IP
		err  error
	}
	pending := make(map[string][]chan *resolverResult, 10)
	notify := make(chan *result, 10)

	for {
		select {
		case req := <-r.req:

			// Add client to wait list
			p, found := pending[req.addr]
			p = append(p, req.ch)
			pending[req.addr] = p

			// Resolve domain
			if !found {
				go func(addr string) {

					// RESOLVE
					ip, err := resolver.ResolveIPv4(addr)

					// Add to cache
					if err == nil {
						r.cacheWrite(addr, ip)
					}

					// Send response to client
					notify <- &result{
						addr: addr,
						ip:   ip,
						err:  err,
					}
				}(req.addr)
			}
		case result := <-notify:
			if clients, found := pending[result.addr]; found {
				rr := &resolverResult{
					ip:  result.ip,
					err: result.err,
				}
				for _, ch := range clients {
					ch <- rr
				}
			} else {
				log.Error(r, "no clients found for domain [%s]", result.addr)
			}
			delete(pending, result.addr)
		}
	}
}

func (r *Resolver) resolve(addr string) (net.IP, bool, error) {
	// Try to parse 'addr' as IP address, and return the result directly in case of success
	if ip := net.ParseIP(addr); ip != nil {
		return ip, false, nil
	}

	// Is cache enabled ?
	if r.cache == nil {
		ip, err := resolver.ResolveIPv4(addr)
		if err != nil {
			return nil, false, err
		}
		return ip, false, nil
	}

	// Read from cache
	if ip, found := r.cacheRead(addr); found {
		return ip, true, nil
	}

	// Try to resolve ...
	req := &resolverReq{
		addr: addr,
		ch:   make(chan *resolverResult, 1),
	}

	// Send resolver request
	r.req <- req

	// Wait for result
	result := <-req.ch

	if result.err != nil {
		return nil, false, result.err
	}

	return result.ip, false, nil
}

func (r *Resolver) Resolve(addr string) (net.IP, error) {
	ip, cached, err := r.resolve(addr)
	if err == nil && ip != nil && ip.String() != addr {
		log.Debug(r, "resolved: [%s] --> [%s] (cached: %v)", addr, ip.String(), cached)
	}
	return ip, err
}
