//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"net"
	"strconv"
)

type ProxyCommand uint8

const (
	ProxyCONNECT  = ProxyCommand(1)
	ProxyBIND     = ProxyCommand(2)
	ProxyUDPASSOC = ProxyCommand(3)
)

type ProxyReply uint8

const (
	ProxyReplyErrNone             = ProxyReply(0)
	ProxyReplyErrGeneric          = ProxyReply(1)
	ProxyReplyErrNotAllowed       = ProxyReply(2)
	ProxyReplyErrNetwork          = ProxyReply(3)
	ProxyReplyErrHost             = ProxyReply(4)
	ProxyReplyErrConn             = ProxyReply(5)
	ProxyReplyErrNotSupported     = ProxyReply(7)
	ProxyReplyErrATYPNotSupported = ProxyReply(8)
	ProxyReplyErrInvalid          = ProxyReply(0xFF)
)

type ProxyRequest struct {
	Command uint8
	Addr    string
	Port    int
}

func (r *ProxyRequest) Address() string {
	return net.JoinHostPort(r.Addr, strconv.Itoa(r.Port))
}

func (p *GMproxy) handleCommandCONNECT(conn *Connection, req *ProxyRequest) error {
	req.Command = uint8(ProxyReplyErrNone)
	srv, err := p.pool.Get(req.Address())
	if err != nil {
		log.Error(conn, "cannot read server from pool: %s", err)
		req.Command = uint8(ProxyReplyErrGeneric)
	}

	proxy, err := p.NewProxy(conn, srv, req.Addr, req.Port)
	if err != nil {
		log.Error(conn, "cannot create proxy via [%s] to [%s]: %s", srv.Address(), req.Address(), err)
		req.Command = uint8(ProxyReplyErrNetwork)
	} else {
		if err = conn.SendRequest(req); err == nil {
			// Start the proxy process
			proxy()
		} else {
			log.Error(conn, "cannot send request reply: %s", err)
		}
	}

	return nil
}

func (p *GMproxy) handleCommands(conn *Connection) error {
	req, err := conn.ReadRequest()
	if err != nil {
		return err
	}

	switch ProxyCommand(req.Command) {
	case ProxyCONNECT:
		return p.handleCommandCONNECT(conn, req)
	default:
		log.Warning(conn, "unsupported proxy command: %v", req.Command)
		req.Command = uint8(ProxyReplyErrNotSupported)
		return conn.SendRequest(req)
	}
	return nil
}
