//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"fmt"
	"net"
)

type Dialer interface {
	Connect() error
	Connected() bool
	Close() error
	DialTCP(net string, laddr, raddr *net.TCPAddr) (net.Conn, error)
}

func newDialer(s *Server) (Dialer, error) {
	switch s.conf.GetType() {
	case "ssh":
		return newDialerSSH(s), nil
	case "gmproxy":
		return newDialerGMProxy(s), nil
	default:
		return nil, fmt.Errorf("unknown dialer type: %s", s.conf.GetType())
	}
}
