//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"encoding/hex"
	"io/ioutil"
	"math/rand"
	"os/user"
	"regexp"
	"time"
)

func sshKeyList() ([]string, error) {
	u, err := user.Current()
	if err != nil {
		return nil, err
	}

	dir := u.HomeDir + "/.ssh/"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	var keys []string
	re := regexp.MustCompile(`^id_\w*\d*$`)
	for _, file := range files {
		n := file.Name()
		if re.MatchString(n) {
			log.Debug("found SSH private key: %s", n)
			keys = append(keys, dir+n)
		}
	}

	return keys, nil
}

func HexDump(data []byte) {
	log.Debug("-------------------->")
	log.Debug(hex.Dump(data))
	log.Debug("<--------------------")
}

func RandomString() string {
	source := rand.New(rand.NewSource(time.Now().UnixNano()))
	buf := make([]byte, 32)
	source.Read(buf)
	return hex.EncodeToString(buf)
}
