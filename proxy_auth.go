//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"fmt"
)

type ProxyAuthMetod uint8

const (
	ProxyAuthNone         = ProxyAuthMetod(0)
	ProxyAuthGSSAPI       = ProxyAuthMetod(1)
	ProxyAuthPassword     = ProxyAuthMetod(2)
	ProxyAuthNoAccepTable = ProxyAuthMetod(0xFF)
)

type ProxyAuthStatus uint8

const (
	ProxyAuthSuccess = ProxyAuthStatus(0)
	ProxyAuthFailure = ProxyAuthStatus(1)
)

var (
	ErrProxyAuthError       = fmt.Errorf("user authentication failed due to protocol mismatch")
	ErrProxyAuthFailed      = fmt.Errorf("user authentication failed")
	ErrProxyAuthNoSupported = fmt.Errorf("not supported authentication method")
)

func methodSupported(methods []byte, method ProxyAuthMetod) bool {
	for _, c := range methods {
		if ProxyAuthMetod(c) == method {
			return true
		}
	}

	return false
}

// RFC 1929
func (p *GMproxy) authenticatePassword(conn *Connection) error {
	err := conn.SendAuthMethod(ProxyAuthPassword)
	if err != nil {
		return err
	}

	auth, err := conn.ReadAuthData()
	if err != nil {
		return err
	}

	err = nil
	resp := ProxyAuthSuccess
	if !p.CheckCredentials(conn, auth) {
		err = ErrProxyAuthFailed
		resp = ProxyAuthFailure
	}
	conn.SendAuthStatus(resp)
	return err
}

func (p *GMproxy) authenticateNone(conn *Connection) error {
	return conn.SendAuthMethod(ProxyAuthNone)
}

func (p *GMproxy) Authenticate(conn *Connection) error {
	methods, err := conn.ReadAuthMethods()
	if err != nil {
		return err
	}

	// For now we support only two methods: 'none' and 'password'
	method := ProxyAuthNone
	if !p.conf.Core.Auth.Empty() {
		method = ProxyAuthPassword
	}

	if !methodSupported(methods, method) {
		method = ProxyAuthNoAccepTable
	}

	switch method {
	case ProxyAuthNone:
		return p.authenticateNone(conn)
	case ProxyAuthPassword:
		return p.authenticatePassword(conn)
	default:
		log.Debug(conn, "no supported auth methods found: %+v", methods)
		conn.SendAuthMethod(ProxyAuthNoAccepTable)
		return ErrProxyAuthNoSupported
	}
}

func (p *GMproxy) CheckCredentials(conn *Connection, auth *AuthBase) bool {
	return p.conf.Core.Auth.Compare(auth)
}
