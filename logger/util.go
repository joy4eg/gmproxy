//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package logger

import (
	"bytes"
	"strings"
)

func getLogTag(args ...interface{}) (*string, []interface{}) {
	if len(args) < 1 {
		return nil, args
	}

	switch t := args[0].(type) {
	case Tagger:
		{
			tag := t.Tag()
			return &tag, args[1:]
		}
	}

	return nil, args
}

func logTag(args ...interface{}) []interface{} {
	t, vals := getLogTag(args...)
	if t != nil {
		return append([]interface{}{*t}, vals)
	} else {
		return vals
	}
}

func logTagFormat(args ...interface{}) (string, []interface{}) {
	tag, vals := getLogTag(args...)

	if len(vals) > 0 {
		switch t := vals[0].(type) {
		case string:
			{
				var buf bytes.Buffer
				if tag != nil {
					s := strings.Replace(*tag, "%", "%%", -1)
					buf.WriteString(s)
					buf.WriteString(" ")
				}
				buf.WriteString(t)
				return buf.String(), vals[1:]
			}
		default:
			{
				return "<missing format string>", vals
			}
		}
	}

	if tag != nil {
		return *tag, vals
	}

	return "<unknown string>", vals
}
