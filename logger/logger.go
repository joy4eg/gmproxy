//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package logger

import (
	"io"
	"os"

	"github.com/op/go-logging"
)

func New(name string, out io.Writer, fmt string) *Logger {
	// msg format
	format := logging.MustStringFormatter(fmt)

	// logger instance
	log := logging.MustGetLogger(name)

	// backend
	backend := logging.NewLogBackend(out, "", 0)

	// backend configure
	backendFormatter := logging.NewBackendFormatter(backend, format)
	backendLeveled := logging.AddModuleLevel(backendFormatter)

	// logger configure
	log.SetBackend(backendLeveled)
	return &Logger{
		log: log,
	}
}

func NewStderr(name string) *Logger {
	return New(name, os.Stderr, formatStderr)
}

type printCbMulti func(format string, args ...interface{})
type printCbSingle func(args ...interface{})

func (l *Logger) print(s printCbSingle, m printCbMulti, args ...interface{}) {
	if len(args) > 1 {
		format, args := logTagFormat(args...)
		m(format, args...)
	} else {
		args = logTag(args...)
		s(args...)
	}
}

func (l *Logger) Info(args ...interface{}) {
	l.print(l.log.Info, l.log.Infof, args...)
}

func (l *Logger) Notice(args ...interface{}) {
	l.print(l.log.Notice, l.log.Noticef, args...)
}

func (l *Logger) Warning(args ...interface{}) {
	l.print(l.log.Warning, l.log.Warningf, args...)
}

func (l *Logger) Error(args ...interface{}) {
	l.print(l.log.Error, l.log.Errorf, args...)
}

func (l *Logger) Critical(args ...interface{}) {
	l.print(l.log.Critical, l.log.Criticalf, args...)
}

func (l *Logger) Debug(args ...interface{}) {
	l.print(l.log.Debug, l.log.Debugf, args...)
}

func (l *Logger) Fatal(args ...interface{}) {
	l.print(l.log.Fatal, l.log.Fatalf, args...)
}
