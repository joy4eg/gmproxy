//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"fmt"
	"sync"

	"github.com/lafikl/consistent"
)

type Pool struct {
	conf     *Config
	servers  map[string]*Server
	balancer *consistent.Consistent
}

func NewPool(c *Config) *Pool {
	return &Pool{
		servers:  make(map[string]*Server, 3),
		conf:     c,
		balancer: consistent.New(),
	}
}

func (p *Pool) Tag() string {
	return "Pool:"
}

func (p *Pool) Empty() bool {
	_, err := p.balancer.Get("gmproxy")
	return err == consistent.ErrNoHosts
}

func (p *Pool) Dead(addr string) {
	log.Debug(p, "dead: %v", addr)

	// Remove from balancer's pool
	defer p.balancer.Remove(addr)
}

func (p *Pool) Alive(addr string) {
	log.Debug(p, "alive: %v", addr)
	p.balancer.Add(addr)
}

func (p *Pool) serverConnect(s *Server) (err error) {
	if s.Connected() {
		return nil
	}

	if err = s.Connect(); err != nil {
		p.Dead(s.Address())
	} else {
		p.Alive(s.Address())
	}

	return err
}

func (p *Pool) serverConnectWithLock(s *Server) error {
	s.Lock()
	defer s.Unlock()
	return p.serverConnect(s)
}

func (p *Pool) getServer(cookie string) (*Server, error) {
	var srv *Server

	addr, err := p.balancer.Get(cookie)
	if err == nil {
		if s, ok := p.servers[addr]; ok {
			srv = s
		} else {
			return nil, fmt.Errorf("cannot find server by address %v: WTF ?!", addr)
		}
	} else {
		return nil, err
	}

	return srv, nil
}

func (p *Pool) getServerFast(cookie string) (*Server, error) {
	// In Fast mode, all servers from the pool are in 'CONNECTED' state
	// So just pick the first one from balancer
	return p.getServer(cookie)
}

func (p *Pool) getServerLazy(cookie string) (*Server, error) {
	// in Lazy mode, we have all configured servers in pool
	// but their state may be 'CONNECTED', or 'NOT CONNECTED'
	srv, err := p.getServer(cookie)
	if err != nil {
		// Looks line all servers are dead ...
		return nil, err
	}

	// We should ensure, that server is in 'CONNECTED' state
	if err := p.serverConnectWithLock(srv); err == nil {
		// Bingo
		return srv, nil
	}

	// Selected server is dead
	// Try to find another via recursion :)
	return p.getServerLazy(cookie)
}

func (p *Pool) get(cookie string) (srv *Server, err error) {
	if p.conf.Core.Lazy {
		srv, err = p.getServerLazy(cookie)
	} else {
		srv, err = p.getServerFast(cookie)
	}

	if err == consistent.ErrNoHosts {
		err = fmt.Errorf("no alive servers found")
		log.Notice(p, "looks like we don't have any alive servers now ...")
	}

	return srv, err
}

func (p *Pool) Get(cookie string) (*Server, error) {
	if p.conf.Core.Matching == "random" {
		cookie = RandomString()
	}

	return p.get(cookie)
}

func (p *Pool) serverFail(srv *Server) {
	srv.failInc()
}

func (p *Pool) Run() {
	var wg sync.WaitGroup

	if p.conf.Core.Lazy {
		for _, srv := range p.servers {
			p.Alive(srv.Address())
		}
		return
	}

	wg.Add(len(p.servers))
	for _, srv := range p.servers {
		go func(s *Server) {
			err := p.serverConnect(s)
			if err != nil {
				log.Warning(p, "cannot connect to %s: %v", s.Address(), err)
			}
			wg.Done()
		}(srv)
	}
	wg.Wait()
}

func (p *Pool) Add(conf *ConfigProxy) {
	srv, err := newServer(conf)
	if err != nil {
		log.Error(p, "cannot add server: %v", err)
	} else {
		p.servers[srv.Address()] = srv
	}
}

func (p *Pool) AddOwn() {
	const t = "gmproxy"
	conf := &ConfigProxy{
		Type:    t,
		Address: t,
	}
	p.Add(conf)

	// In not lazy mode mark our server as 'alive' force
	if !p.conf.Core.Lazy {
		p.Alive(t)
	}
}
