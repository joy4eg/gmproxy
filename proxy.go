//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"fmt"
	"io"
	"net"
	"sync"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/hako/durafmt"
)

const (
	SOCKSver4 = uint8(4)
	SOCKSver5 = uint8(5)
)

type GMproxy struct {
	conf     *Config
	pool     *Pool
	resolver *Resolver
}

func New(conf *Config) (*GMproxy, error) {
	if err := conf.validate(); err != nil {
		log.Fatal("config error: %s", err)
	}
	p := &GMproxy{
		conf:     conf,
		pool:     NewPool(conf),
		resolver: NewResolver(conf),
	}

	return p, nil
}

func (p *GMproxy) Tag() string {
	return "GMProxy:"
}

func (p *GMproxy) poolBuild() {
	log.Debug("building pool of servers ...")
	for i, _ := range p.conf.Proxy {
		p.pool.Add(&p.conf.Proxy[i])
	}

	if p.conf.Core.Own {
		p.pool.AddOwn()
	} else if len(p.conf.Proxy) == 0 {
		log.Fatal(p, "no proxy servers are configured")
	}

	p.pool.Run()
	if p.pool.Empty() {
		log.Fatal(p, "pool is empty")
	}
}

func (p *GMproxy) Run() error {
	p.poolBuild()
	log.Info(p, "starting on [TCP %s] ...", p.conf.Core.ListenAddr)
	if p.conf.Core.Auth.Empty() {
		log.Info(p, "authentication: not set")
	} else {
		log.Info(p, "authentication: configured")
	}
	return p.ListenAndServe("tcp", p.conf.Core.ListenAddr)
}

func (p *GMproxy) ListenAndServe(network, addr string) error {
	l, err := net.Listen(network, addr)
	if err != nil {
		return err
	}
	return p.Serve(l)
}

func (p *GMproxy) Serve(l net.Listener) error {
	for {
		conn, err := l.Accept()
		if err != nil {
			return err
		}
		log.Debug(p, "new connection from [%s]", conn.RemoteAddr())
		t, _ := conn.(*net.TCPConn)
		t.SetKeepAlive(true)
		go p.ServeConn(NewConnection(conn))
	}
	return nil
}

func (p *GMproxy) serveConn5(conn *Connection) {
	err := p.Authenticate(conn)
	if err != nil {
		log.Error(conn, "failed to authenticate: %s", err)
		return

	}
	if !p.conf.Core.Auth.Empty() {
		log.Debug(conn, "authenticated")
	}
	err = p.handleCommands(conn)
	if err != nil {
		log.Error(conn, "cannot process command: %s", err)
	}
}

func (p *GMproxy) ServeConn(conn *Connection) error {
	var version uint8
	var err error
	defer func() {
		if r := recover(); r != nil {
			log.Warning(conn, "recovered: %v", r)
		}
	}()
	defer conn.Close()

	// Read version byte
	if version, err = conn.ReadVersion(); err != nil {
		log.Error(conn, "failed to get version byte: %v", err)
		return err
	}
	switch version {
	case SOCKSver5:
		p.serveConn5(conn)
	default:
		log.Error(conn, "unsupported SOCKS version: %v", version)
	}

	return nil
}

func (p *GMproxy) transfer(in, out net.Conn) {
	var (
		sent     uint64
		received uint64
		wg       sync.WaitGroup
		t        time.Time
	)

	wg.Add(2)
	t = time.Now()
	f := func(in, out net.Conn, wg *sync.WaitGroup, stat *uint64) {
		n, _ := io.Copy(out, in)
		*stat = uint64(n)
		// In any case we close both connections
		// to let io.Copy() know that it should stop
		in.Close()
		out.Close()
		wg.Done()
	}

	/* TX */
	go f(in, out, &wg, &sent)

	/* RX */
	f(out, in, &wg, &received)

	wg.Wait()
	df := durafmt.Parse(time.Since(t))
	log.Debug(in, "transfer done: sent: [%s], received: [%s], duration: [%s]", humanize.Bytes(sent), humanize.Bytes(received), df)
}

func (p *GMproxy) NewProxy(conn *Connection, server *Server, address string, port int) (func(), error) {
	ip, err := p.resolver.Resolve(address)
	if err != nil {
		return nil, fmt.Errorf("cannot resolve: %s: %s", address, err)
	}
	remote, err := server.DialTCP("tcp", conn.RemoteAddr().(*net.TCPAddr), &net.TCPAddr{IP: ip, Port: port})
	if err != nil {
		p.pool.serverFail(server)
		return nil, err
	}
	return func() {
		log.Debug(p, "starting proxying from [%s] via [%s] to [%s:%d]", conn.RemoteAddr(), server.Address(), address, port)
		p.transfer(conn, remote)
	}, nil
}
