//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

type AuthInterface interface {
	Empty() bool
	GetUsername() string
	GetPassword() string
	GetKeys() []string
	String() string
}

type AuthBase struct {
	Username string `yaml:"username"`
	Password string `yaml:"password"`
}

func (b *AuthBase) GetUsername() string {
	return b.Username
}

func (b *AuthBase) GetPassword() string {
	return b.Password
}

func (b *AuthBase) Empty() bool {
	return b.Username == "" && b.Password == ""
}

func (b *AuthBase) Compare(n *AuthBase) bool {
	return b.Username == n.Username && b.Password == n.Password
}

func (b *AuthBase) String() string {
	var p string
	var l string

	if b.Password != "" {
		p = b.Password
	} else {
		p = "<not set>"
	}

	if b.Username != "" {
		l = b.Username
	} else {
		l = "<not set>"
	}

	return fmt.Sprintf("username:%s, password:%s", l, p)
}

func (b *AuthBase) fillFromParent(parent *AuthBase) {
	if b.Password == "" {
		b.Password = parent.Password
	}
	if b.Username == "" {
		b.Username = parent.Username
	}
}

func (s *AuthBase) GetKeys() []string {
	return nil
}

type AuthSSH struct {
	AuthBase `yaml:",inline"`
	Keys     []string `yaml:"keys"`
}

func (s *AuthSSH) GetKeys() []string {
	return s.Keys
}

func (s *AuthSSH) String() string {
	str := s.AuthBase.String()

	if len(s.Keys) > 0 {
		var k []string
		for _, key := range s.Keys {
			k = append(k, "key:"+key)
		}
		str += ", " + strings.Join(k, ", ")
	}
	return str
}

func (s *AuthSSH) fillFromParent(parent *AuthSSH) {
	s.AuthBase.fillFromParent(&parent.AuthBase)
	if len(s.Keys) == 0 {
		s.Keys = parent.Keys
	}
}

type ConfigCore struct {
	ListenAddr string   `yaml:"listen"`
	Matching   string   `yaml:"matching"`
	Lazy       bool     `yaml:"lazy"`
	Own        bool     `yaml:"own"`
	Interval   int      `yaml:"check_interval"`
	Auth       AuthBase `yaml:"auth"`
}

type ConfigDNS struct {
	Cache   bool     `yaml:"cache"`
	Timeout int      `yaml:"timeout"`
	Servers []string `yaml:"nameserver"`
}

type ConfigSSH struct {
	Port int     `yaml:"port"`
	Auth AuthSSH `yaml:"auth"`
}

type ConfigSOCKS struct {
	Auth AuthBase `yaml:"auth"`
}

type ConfigProxy struct {
	Type    string
	Address string
	Auth    AuthInterface
}

func (c *ConfigProxy) GetType() string {
	return strings.ToLower(c.Type)
}

func (c *ConfigProxy) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type conf struct {
		Type    string  `yaml:"type"`
		Address string  `yaml:"address"`
		Auth    AuthSSH `yaml:"auth"`
	}

	proxy := &conf{
		Type: "ssh",
	}
	if err := unmarshal(&proxy); err != nil {
		return err
	}

	c.Type = proxy.Type
	c.Address = proxy.Address
	c.Auth = nil

	switch c.GetType() {
	case "ssh":
		if p := &proxy.Auth; len(p.Keys) > 0 || p.Username != "" || p.Password != "" {
			c.Auth = p
		}
	case "socks":
		if p := &proxy.Auth.AuthBase; p.Username != "" || p.Password != "" {
			c.Auth = p
		}
	default:
		return fmt.Errorf("unknown proxy type: %s", c.Type)
	}

	return nil
}

type Config struct {
	Core  ConfigCore    `yaml:"core"`
	DNS   ConfigDNS     `yaml:"dns"`
	SSH   ConfigSSH     `yaml:"ssh"`
	SOCKS ConfigSOCKS   `yaml:"socks"`
	Proxy []ConfigProxy `yaml:"proxy"`
}

func (c *Config) Dump() {
	log.Debug("--- Config dump --->>")
	log.Debug("Core:  %+v", c.Core)
	log.Debug("DNS:   %+v", c.DNS)
	log.Debug("SSH:   %+v", c.SSH)
	log.Debug("SOCKS: %+v", c.SOCKS)
	for _, p := range c.Proxy {
		log.Debug("Proxy [%s]:", p.GetType())
		log.Debug("\tAddress: [%s]", p.Address)
		log.Debug("\tAuth:    [%s]", p.Auth)
	}
	if c.Core.Own {
		log.Notice("Our own proxy server is enabled")
	}
	log.Debug("<<--- Config dump ---")
}

func ConfigDefault() *Config {
	conf := &Config{
		Core: ConfigCore{
			ListenAddr: "127.0.0.1:7799",
			Matching:   "static",
			Lazy:       true,
			Own:        false,
			Interval:   60, // 1 minute by defaut
		},
		SSH: ConfigSSH{
			Port: 22,
		},
		DNS: ConfigDNS{
			Cache:   true,
			Timeout: 5,
			Servers: []string{"8.8.8.8", "8.8.4,4"},
		},
	}

	return conf
}

func ConfigParse(path string) (*Config, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("cannot read config file: %s", err.Error())
	}

	conf := ConfigDefault()
	err = yaml.Unmarshal(data, conf)
	if err != nil {
		return nil, fmt.Errorf("cannot parse config file: %s: %s", path, err.Error())
	}

	// Check SSH keys
	if len(conf.SSH.Auth.Keys) == 0 {
		k, err := sshKeyList()
		if err != nil {
			return nil, fmt.Errorf("cannot read SSH directory: %s", err)
		} else if len(k) == 0 && conf.SSH.Auth.Empty() {
			return nil, fmt.Errorf("no SSH keys found and no passwords provided")
		}
		conf.SSH.Auth.Keys = k
	}

	// Now merge 'auth' data from global config to the each child
	for idx := range conf.Proxy {
		child := &conf.Proxy[idx]
		switch child.GetType() {
		case "ssh":
			if !strings.Contains(child.Address, ":") {
				child.Address += ":" + strconv.Itoa(conf.SSH.Port)
			}
			if child.Auth == nil {
				child.Auth = &conf.SSH.Auth
			} else {
				a, _ := child.Auth.(*AuthSSH)
				a.fillFromParent(&conf.SSH.Auth)
			}
		case "socks":
			if child.Auth == nil {
				child.Auth = &conf.SOCKS.Auth
			} else {
				a, _ := child.Auth.(*AuthBase)
				a.fillFromParent(&conf.SOCKS.Auth)
			}
		default:
			log.Notice("unknown child type: %s", child.Type)
		}
	}
	return conf, nil
}

func (c *Config) validate() error {
	if c.Core.ListenAddr == "" {
		return fmt.Errorf("listen address is not set")
	}
	return nil
}
