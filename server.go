//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"fmt"
	"sync"
)

type Server struct {
	sync.Mutex
	Dialer
	fail int
	conf *ConfigProxy
}

func newServer(conf *ConfigProxy) (*Server, error) {
	srv := &Server{conf: conf}
	d, err := newDialer(srv)
	if err != nil {
		return nil, fmt.Errorf("cannot create server %s: %s", srv.Address(), err)
	}
	srv.Dialer = d
	return srv, nil
}

func (s *Server) GetAuthData() AuthInterface {
	return s.conf.Auth
}

func (s *Server) Address() string {
	return s.conf.Address
}

func (s *Server) failInc() int {
	s.Lock()
	defer s.Unlock()

	s.fail++
	return s.fail
}
