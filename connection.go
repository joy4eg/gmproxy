//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"net"
)

type ProxyAddressType uint8

const (
	ProxyATYPIPv4   = ProxyAddressType(1)
	ProxyATYPDomain = ProxyAddressType(3)
	ProxyATYPIPv6   = ProxyAddressType(4)
)

type Connection struct {
	net.Conn
}

func NewConnection(conn net.Conn) *Connection {
	return &Connection{
		Conn: conn,
	}
}

func (c *Connection) Tag() string {
	return "CONN-" + c.RemoteAddr().String() + ":"
}

func (c *Connection) ReadByte() (uint8, error) {
	one := []byte{0}

	if _, err := c.Read(one); err != nil {
		return 0, err
	}

	return one[0], nil
}

func (c *Connection) ReadVersion() (uint8, error) {
	return c.ReadByte()
}

func (c *Connection) ReadAuthMethods() ([]byte, error) {
	header := []byte{0}
	if _, err := c.Read(header); err != nil {
		return nil, err
	}

	n := int(header[0])

	// Sanity check
	if n > 0xFF {
		return nil, fmt.Errorf("too many authentication methods: %d", n)
	}

	methods := make([]byte, n)
	_, err := io.ReadAtLeast(c, methods, n)
	return methods, err
}

func (c *Connection) ReadAuthData() (*AuthBase, error) {
	header := []byte{0, 0}
	if _, err := io.ReadAtLeast(c, header, 2); err != nil {
		return nil, err
	}

	// Be RFC friendly
	if header[0] != 1 {
		return nil, ErrProxyAuthError
	}

	// Username length
	userLen := int(header[1])

	// Sanity check
	if userLen > 0xFF {
		return nil, ErrProxyAuthError
	}
	user := make([]byte, userLen)
	if _, err := io.ReadAtLeast(c, user, userLen); err != nil {
		return nil, err
	}

	// Next field
	if _, err := c.Read(header[:1]); err != nil {
		return nil, err
	}

	// Password length
	passLen := int(header[0])

	// Sanity check
	if passLen > 0xFF {
		return nil, ErrProxyAuthError
	}

	pass := make([]byte, passLen)
	if _, err := io.ReadAtLeast(c, pass, passLen); err != nil {
		return nil, err
	}

	return &AuthBase{
		Username: string(user),
		Password: string(pass),
	}, nil
}

func (c *Connection) SendAuthMethod(method ProxyAuthMetod) error {
	_, err := c.Write([]byte{SOCKSver5, byte(method)})
	return err
}

func (c *Connection) SendAuthStatus(status ProxyAuthStatus) error {
	_, err := c.Write([]byte{1, byte(status)})
	return err
}

func (c *Connection) ReadRequest() (*ProxyRequest, error) {
	request := &ProxyRequest{}
	header := []byte{0, 0, 0, 0}
	if _, err := io.ReadAtLeast(c, header, 4); err != nil {
		return nil, err
	}

	if header[0] != SOCKSver5 {
		return nil, fmt.Errorf("unknown SOCKS version: %v", header[0])
	}
	request.Command = header[1]
	// header[2] is reserved and not used
	switch ProxyAddressType(header[3]) {
	case ProxyATYPIPv4:
		addr := make([]byte, 4)
		if _, err := io.ReadAtLeast(c, addr, len(addr)); err != nil {
			return nil, err
		}
		request.Addr = net.IP(addr).String()
	case ProxyATYPIPv6:
		addr := make([]byte, 16)
		if _, err := io.ReadAtLeast(c, addr, len(addr)); err != nil {
			return nil, err
		}
		request.Addr = net.IP(addr).String()
	case ProxyATYPDomain:
		domainLen, err := c.ReadByte()
		if err != nil {
			return nil, err
		}
		// Sanity check
		if domainLen > 0xFF {
			return nil, fmt.Errorf("domain name is too long")
		}
		addr := make([]byte, domainLen)
		if _, err := io.ReadAtLeast(c, addr, int(domainLen)); err != nil {
			return nil, err
		}
		request.Addr = string(addr)
	default:
		return nil, fmt.Errorf("unknown address type: %v", header[3])
	}

	// Read port
	port := []byte{0, 0}
	if _, err := io.ReadAtLeast(c, port, 2); err != nil {
		return nil, err
	}
	request.Port = (int(port[0]) << 8) | int(port[1])
	return request, nil
}

func writeAddressFromReq(buf *bytes.Buffer, req *ProxyRequest) {
	var atyp ProxyAddressType
	var domainLen uint8
	var address []byte

	if ip := net.ParseIP(req.Addr); ip != nil {
		if v4 := ip.To4(); v4 != nil {
			atyp = ProxyATYPIPv4
			address = make([]byte, 4)
			copy(address, v4)
		} else {
			atyp = ProxyATYPIPv6
			address = make([]byte, 16)
			copy(address, ip.To16())
		}
	} else {
		atyp = ProxyATYPDomain
		domainLen = uint8(len(req.Addr))
		address = []byte(req.Addr)
	}

	// Address type
	binary.Write(buf, binary.BigEndian, atyp)

	if atyp == ProxyATYPDomain {
		// Domain name length
		binary.Write(buf, binary.BigEndian, domainLen)
	}

	// Address
	binary.Write(buf, binary.BigEndian, address)

	// Port
	binary.Write(buf, binary.BigEndian, uint16(req.Port))
}

func (c *Connection) SendRequest(req *ProxyRequest) error {
	var buf bytes.Buffer

	// SOCKS version
	binary.Write(&buf, binary.BigEndian, SOCKSver5)

	// Reply code
	binary.Write(&buf, binary.BigEndian, req.Command)

	// Reserved field
	binary.Write(&buf, binary.BigEndian, uint8(0))

	// Address
	writeAddressFromReq(&buf, req)

	// Send
	_, err := buf.WriteTo(c)
	return err
}
