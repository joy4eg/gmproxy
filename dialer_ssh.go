//
//   Copyright 2017 Yaroslav Syrytsia <me@ys.lc>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package gmproxy

import (
	"fmt"
	"io/ioutil"
	"net"
	"time"

	"golang.org/x/crypto/ssh"
)

const dialerSSHTimeout = 45 * time.Second

type DialerSSH struct {
	server *Server
	conn   *ssh.Client
}

func newDialerSSH(s *Server) *DialerSSH {
	return &DialerSSH{
		server: s,
	}
}

func (d *DialerSSH) Tag() string {
	return "Dialer-SSH-" + d.server.Address() + ":"
}

func (d *DialerSSH) readPrivateKeys() []ssh.Signer {
	var signers []ssh.Signer

	for _, file := range d.server.GetAuthData().GetKeys() {
		key, err := ioutil.ReadFile(file)
		if err != nil {
			log.Warning(d, "cannot read private key: %v", err)
			continue
		}
		signer, err := ssh.ParsePrivateKey(key)
		if err != nil {
			log.Error(d, "unable to parse private key: %v", err)
			continue
		}
		signers = append(signers, signer)
	}

	return signers
}

func (d *DialerSSH) getConfig() (*ssh.ClientConfig, error) {
	auth := d.server.GetAuthData()
	methods := []ssh.AuthMethod{}

	// Add 'password', if present
	if pass := auth.GetPassword(); pass != "" {
		methods = append(methods, ssh.Password(pass))
	}

	// Add 'keys', if present
	if signers := d.readPrivateKeys(); len(signers) > 0 {
		methods = append(methods, ssh.PublicKeys(signers...))
	}

	if len(methods) == 0 {
		return nil, fmt.Errorf("no authentication methods configured")
	}

	conf := &ssh.ClientConfig{
		User: auth.GetUsername(),
		Auth: methods,

		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			return nil
		},

		Timeout: dialerSSHTimeout,
	}
	return conf, nil
}

func (d *DialerSSH) Connected() bool {
	return d.conn != nil
}

func (d *DialerSSH) Close() error {
	if !d.Connected() {
		return nil
	}

	err := d.conn.Close()
	d.conn = nil
	return err
}

func (d *DialerSSH) DialTCP(net string, laddr, raddr *net.TCPAddr) (net.Conn, error) {
	if !d.Connected() {
		return nil, fmt.Errorf("server is not connected")
	}
	return d.conn.DialTCP(net, laddr, raddr)
}

func (d *DialerSSH) Connect() error {
	log.Debug(d, "connecting ...")
	conf, err := d.getConfig()
	if err != nil {
		return err
	}
	conn, err := ssh.Dial("tcp", d.server.Address(), conf)
	if err != nil {
		return err
	}

	log.Debug(d, "connected")
	d.conn = conn
	return nil
}
